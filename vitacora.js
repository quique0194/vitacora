angular.module('vitacoraApp', []).

controller('vitacoraCtrl', [
    '$scope',
    function ($scope) {
        $scope.conversation = {
            name: "Topicos de IA"
        };
        $scope.comments = [
            {
                who: "Jeffrey",
                what: "Hola!"
            },
            {
                who: "Kike",
                what: "Hala!"
            },
            {
                who: "Jeffrey",
                what: "q haciendo?"
            },
            {
                who: "Kike",
                what: "Aca con topicos"
            },
            {
                who: "Jeffrey",
                what: "puuuucha xD"
            },
        ];
        navigator.getUserMedia  = navigator.getUserMedia ||
                                  navigator.webkitGetUserMedia ||
                                  navigator.mozGetUserMedia ||
                                  navigator.msGetUserMedia;
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        var audioContext = new AudioContext();
        var analyserContext = null;

        function updateAnalysers(time) {
            if (!analyserContext) {
                var canvas = document.getElementById("analyzer");
                canvasWidth = canvas.width;
                canvasHeight = canvas.height;
                analyserContext = canvas.getContext('2d');
            }

            // analyzer draw code here
            {
                var SPACING = 3;
                var BAR_WIDTH = 1;
                var numBars = Math.round(canvasWidth / SPACING);
                var freqByteData = new Uint8Array(analyserNode.frequencyBinCount);

                analyserNode.getByteFrequencyData(freqByteData); 

                analyserContext.clearRect(0, 0, canvasWidth, canvasHeight);
                analyserContext.fillStyle = '#F6D565';
                analyserContext.lineCap = 'round';
                var multiplier = analyserNode.frequencyBinCount / numBars;

                // Draw rectangle for each frequency bin.
                for (var i = 0; i < numBars; ++i) {
                    var magnitude = 0;
                    var offset = Math.floor( i * multiplier );
                    // gotta sum/average the block, or we miss narrow-bandwidth spikes
                    for (var j = 0; j< multiplier; j++)
                        magnitude += freqByteData[offset + j];
                    magnitude = magnitude / multiplier;
                    var magnitude2 = freqByteData[i * multiplier];
                    analyserContext.fillStyle = "hsl( " + Math.round((i*360)/numBars) + ", 100%, 50%)";
                    analyserContext.fillRect(i * SPACING, canvasHeight, BAR_WIDTH, -magnitude);
                }
            }
            
            rafID = window.requestAnimationFrame( updateAnalysers );
        }


        if (navigator.getUserMedia) {
          navigator.getUserMedia({audio: true}, 
            function accepted(stream) {
                inputPoint = audioContext.createGain();

                // Create an AudioNode from the stream.
                realAudioInput = audioContext.createMediaStreamSource(stream);
                audioInput = realAudioInput;
                audioInput.connect(inputPoint);

                analyserNode = audioContext.createAnalyser();
                analyserNode.fftSize = 2048;
                inputPoint.connect( analyserNode );

                audioRecorder = new Recorder( inputPoint );

                zeroGain = audioContext.createGain();
                zeroGain.gain.value = 0.0;
                inputPoint.connect( zeroGain );
                zeroGain.connect( audioContext.destination );
                updateAnalysers();
            },
            function rejected(e) {
                console.log("Rejected!");
                console.log(e);
            });
        } else {
            video.src = 'somevideo.webm'; // fallback.
        }
    }
]);
